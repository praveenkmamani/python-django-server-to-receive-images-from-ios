# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0006_auto_20150715_1248'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='drawing',
            name='photo',
        ),
        migrations.AddField(
            model_name='drawing',
            name='image',
            field=models.FileField(default='', upload_to=b'img'),
            preserve_default=False,
        ),
    ]
