# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0013_drawing_likes'),
    ]

    operations = [
        migrations.AddField(
            model_name='drawing',
            name='userID',
            field=models.CharField(default=b'', max_length=64),
            preserve_default=True,
        ),
    ]
