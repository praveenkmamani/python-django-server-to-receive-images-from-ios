from django.db import models                       
from django.conf import settings

class Drawing(models.Model):
	image = models.FileField(upload_to='img')
	likes = models.IntegerField(default=0)
	userID = models.CharField(max_length=64, default='')
	newLikes = models.IntegerField(default=0)
	shouldNotify = models.BooleanField(default=False)
       	class Meta:
		ordering = ["-id"]
	def __str__(self):
		return self.image.url

class Comment(models.Model):
	drawing = models.ForeignKey(Drawing, default='')
	comment = models.CharField(max_length=60, default='')
	isNotified = models.BooleanField(default=False)
	def __str__(self):
		return self.comment.encode('UTF-8', 'replace')
