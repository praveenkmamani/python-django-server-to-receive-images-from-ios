# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0007_auto_20150716_0941'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='drawing',
            options={'ordering': ['created']},
        ),
        migrations.AddField(
            model_name='drawing',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 23, 5, 39, 4, 251807, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
