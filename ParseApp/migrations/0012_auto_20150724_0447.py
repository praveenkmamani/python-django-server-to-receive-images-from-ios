# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0011_auto_20150724_0444'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='drawing',
            options={'ordering': ['-id']},
        ),
    ]
