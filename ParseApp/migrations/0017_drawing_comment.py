# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0016_auto_20150814_1044'),
    ]

    operations = [
        migrations.AddField(
            model_name='drawing',
            name='comment',
            field=models.CharField(default=b'', max_length=144),
            preserve_default=True,
        ),
    ]
