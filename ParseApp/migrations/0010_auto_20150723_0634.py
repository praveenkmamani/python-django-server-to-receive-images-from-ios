# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0009_auto_20150723_0630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drawing',
            name='created',
            field=models.DateTimeField(verbose_name=b'date published'),
            preserve_default=True,
        ),
    ]
