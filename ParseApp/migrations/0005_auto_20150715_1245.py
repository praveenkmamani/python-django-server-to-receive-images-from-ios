# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0004_auto_20150715_0839'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drawing',
            name='photo',
            field=models.FileField(upload_to=b'/Users/Akira/binaryParser/media/'),
            preserve_default=True,
        ),
    ]
