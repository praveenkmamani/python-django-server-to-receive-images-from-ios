# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0015_drawing_flag'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='drawing',
            name='flag',
        ),
        migrations.AddField(
            model_name='drawing',
            name='newLikes',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
