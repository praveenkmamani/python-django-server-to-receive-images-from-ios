# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0012_auto_20150724_0447'),
    ]

    operations = [
        migrations.AddField(
            model_name='drawing',
            name='likes',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
