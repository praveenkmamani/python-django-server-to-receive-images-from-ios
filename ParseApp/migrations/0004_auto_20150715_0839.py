# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0003_auto_20150715_0726'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drawing',
            name='photo',
            field=models.FileField(upload_to=b''),
            preserve_default=True,
        ),
    ]
