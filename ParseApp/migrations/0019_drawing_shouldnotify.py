# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0018_auto_20151019_1131'),
    ]

    operations = [
        migrations.AddField(
            model_name='drawing',
            name='shouldNotify',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
