from django.conf.urls import patterns, url
from ParseApp import views

urlpatterns = patterns('', 
		url(r'^list/$', views.list, name='list'),
		url(r'^board/$', views.board, name='board'),
		url(r'^add_likes/$', views.add_likes, name='add_likes'),
		url(r'^get_userID/$', views.get_userID, name='get_userID'),
		url(r'^get_images_count/$', views.get_images_count, name='get_images_count'),
		url(r'^get_players_images/$', views.get_players_images, name='get_players_images'),
		url(r'^notification/$', views.notification, name='notification'),
		url(r'^deleteImage/$', views.deleteImage, name='deleteImage'),
		url(r'^querynew/$', views.querynew, name='querynew'),
		url(r'^queryoldusers/$', views.queryoldusers, name='queryoldusers'),
		url(r'^querynewusers/$', views.querynewusers, name='querynewusers'),
		url(r'^comment/$', views.comment, name='comment'),
		url(r'^get_new_comments/$', views.get_new_comments, name='get_new_comments'),
		url(r'^get_all_comments/$', views.get_all_comments, name='get_all_comments'),
		url(r'^get_likes/$', views.get_likes, name='get_likes'),
		url(r'^get_default_image/$', views.get_default_image, name='get_default_image'),
)
