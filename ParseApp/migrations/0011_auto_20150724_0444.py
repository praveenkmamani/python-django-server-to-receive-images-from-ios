# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0010_auto_20150723_0634'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='drawing',
            options={},
        ),
        migrations.RemoveField(
            model_name='drawing',
            name='created',
        ),
    ]
