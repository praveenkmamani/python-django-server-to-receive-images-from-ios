# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0008_auto_20150723_0539'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='drawing',
            options={'ordering': ['-created']},
        ),
    ]
