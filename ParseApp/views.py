from django.shortcuts import render
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from ParseApp.models import Drawing, Comment
from ParseApp.forms import UploadFileForm 
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from PIL import Image
import uuid
import re

@csrf_exempt
def list(request):
	if request.method == "POST":
		form = UploadFileForm(request.POST, request.FILES)	

		if form.is_valid():
			newImage = Drawing(image = request.FILES['image'], userID = request.META['HTTP_USERID'])
			newImage.save()
			return HttpResponse('hej hej')

@csrf_exempt
def board(request):
	if request.method == "POST":
		response = HttpResponse(content_type="image/png")
		theNumberOfItems = Drawing.objects.count()
		if (theNumberOfItems < 1):
			return response
		ID = request.body
		print ID
		if (int(ID) == 0):
			drawings = Drawing.objects.all()
		else:
			drawings = Drawing.objects.exclude(id__gte=int(ID))

		if (drawings.count() < 1):
			return response
		drawing = drawings.order_by('-id')[0]
		url = settings.BASE_DIR + drawing.image.url
		urlToImage = drawing.image.url
		LikesOfImage = drawing.likes
		response['likes'] = LikesOfImage
		response['url'] = urlToImage
		response['ID'] = drawing.id
		image = Image.open(url)
		size = 104, 104
		image.thumbnail(size, Image.ANTIALIAS)
		image.save(response, 'png')
		return response

@csrf_exempt
def querynew(request):
	if request.method == "POST":
		response = HttpResponse(content_type="image/png")
		theNumberOfItems = Drawing.objects.count()
		if (theNumberOfItems < 1):
			return response
		ID = request.body
		print ID
		if (int(ID) == 0):
			drawings = Drawing.objects.all()
			if (drawings.count() < 1):
				return response
			drawing = drawings.order_by('id')[0]
			url = settings.BASE_DIR + drawing.image.url
			urlToImage = drawing.image.url
			LikesOfImage = drawing.likes
			response['likes'] = LikesOfImage
			response['url'] = urlToImage
			response['ID'] = drawing.id
			image = Image.open(url)
			size = 104, 104
			image.thumbnail(size, Image.ANTIALIAS)
			image.save(response, 'png')
			return response
		else:
			drawings = Drawing.objects.exclude(id__lte=int(ID))
			if (drawings.count() < 1):
				return response
			drawing = drawings.order_by('id')[0]
			url = settings.BASE_DIR + drawing.image.url
			urlToImage = drawing.image.url
			LikesOfImage = drawing.likes
			response['likes'] = LikesOfImage
			response['url'] = urlToImage
			response['ID'] = drawing.id
			image = Image.open(url)
			size = 104, 104
			image.thumbnail(size, Image.ANTIALIAS)
			image.save(response, 'png')
			return response

@csrf_exempt
def queryoldusers(request):
	if request.method == "POST":
		response = HttpResponse(content_type="image/png")
		theNumberOfItems = Drawing.objects.count()
		if (theNumberOfItems < 1):
			return response
		sentString = request.body
		splitString = sentString.split("xxxxx", 1)
		n = splitString[0]
		number = int(n)
		userIDSent = splitString[1]
		drawings = Drawing.objects.filter(userID=userIDSent)
		if (drawings.count() < 1):
			return response
		drawingsSorted = drawings.order_by('-id')
		if (number == 0):                       
			Q = drawingsSorted
		else:
			Q = drawingsSorted.exclude(id__gte=number)
		if (Q.count() < 1):
			return response
		drawing = Q[0]
		url = settings.BASE_DIR + drawing.image.url
		urlToImage = drawing.image.url
		LikesOfImage = drawing.likes
		response['likes'] = LikesOfImage
		response['url'] = urlToImage
		response['ID'] = drawing.id
		image = Image.open(url)
		size = 104, 104
		image.thumbnail(size, Image.ANTIALIAS)
		image.save(response, 'png')
		return response
@csrf_exempt
def querynewusers(request):
	if request.method == "POST":
		response = HttpResponse(content_type="image/png")
		theNumberOfItems = Drawing.objects.count()
		if (theNumberOfItems < 1):
			return response
		sentString = request.body
		splitString = sentString.split("xxxxx", 1)
		n = splitString[0]
		number = int(n)
		userIDSent = splitString[1]
		drawings = Drawing.objects.filter(userID=userIDSent)
		if (drawings.count() < 1):
			return response
		drawingsSorted = drawings.order_by('id')
		if (number == 0):                       
			Q = drawingsSorted
		else:
			Q = drawingsSorted.exclude(id__lte=number)

		if (Q.count() < 1):
			return response
		drawing = Q[0]
		url = settings.BASE_DIR + drawing.image.url
		urlToImage = drawing.image.url
		LikesOfImage = drawing.likes
		response['likes'] = LikesOfImage
		response['url'] = urlToImage
		response['ID'] = drawing.id
		image = Image.open(url)
		size = 104, 104
		image.thumbnail(size, Image.ANTIALIAS)
		image.save(response, 'png')
		return response

@csrf_exempt
def add_likes(request):
	if request.method == "POST":
		i = request.body
		imageURL = i[7:]
		try:
			drawing = Drawing.objects.get(image=imageURL)
		except Drawing.DoesNotExist:
			return HttpResponse('')

		drawing.likes = drawing.likes + 1
		drawing.newLikes = drawing.newLikes + 1
		drawing.shouldNotify = True
		drawing.save()
		return HttpResponse(drawing.likes)

def get_userID(request):
	if request.method == "GET":
		userID = uuid.uuid1()
		return HttpResponse(userID)

@csrf_exempt
def get_images_count(request):
	if request.method == "POST":
                userIDSent = request.body
		print userIDSent
		drawings = Drawing.objects.all()
		count = 0
		for drawing in drawings:
			if drawing.userID == userIDSent:
				count = count + 1
                print count
		return HttpResponse(count)
@csrf_exempt
def get_players_images(request):
	if request.method == "POST":
		response = HttpResponse(content_type="image/png")
		sentString = request.body
		splitString = sentString.split("xxxxx", 1)
		number = splitString[0]
		drawings = Drawing.objects.filter(userID=splitString[1])
		theNumberOfItems = drawings.count()
		if (theNumberOfItems < 1):
			return response
		else:
			imageMaxID = drawings.order_by("-id")[0].id
			print imageMaxID
			imageMinID = drawings.order_by("id")[0].id
			print imageMinID
			theNumberOfItemIndex = drawings.count() - 1
			if (int(number) > theNumberOfItemIndex):
				return response
			else:
				imageID = drawings.order_by("-id")[int(number)].id
				print imageID
				drawing = Drawing.objects.get(id=imageID)
				url = settings.BASE_DIR + drawing.image.url
				urlToImage = drawing.image.url
				likesOfImage = drawing.likes
				image = Image.open(url)
				size = 104, 104
				image.thumbnail(size, Image.ANTIALIAS)
				image.save(response, 'png')
				response['url'] = urlToImage
				response['likes'] = likesOfImage
				drawing.save()
				return response
@csrf_exempt
def notification(request):
	if request.method == "POST":
		response = HttpResponse(content_type="image/png")
		sentUserID = request.body
		drawings = Drawing.objects.filter(userID=sentUserID)
		if (drawings.count() < 1):
			return HttpResponse()
		drawingsToNotify = drawings.filter(shouldNotify=True)
		if (drawingsToNotify.count() < 1):
			return HttpResponse()
                drawingToNotify = drawingsToNotify[0]
		likesToNotify = drawingToNotify.newLikes
		commentsToNotify = drawingToNotify.comment_set.all()
		theNumberOfComments = commentsToNotify.count()

		url = settings.BASE_DIR + drawingToNotify.image.url
		urlToImage = drawingToNotify.image.url
		image = Image.open(url)
		size = 104, 104
		image.thumbnail(size, Image.ANTIALIAS)
		image.save(response, 'png')
		response['url'] = urlToImage
		response['likes'] = likesToNotify
		response['theNumberOfComments'] = theNumberOfComments
		drawingToNotify.newLikes = 0 
		drawingToNotify.shouldNotify = False
		drawingToNotify.save()
		return response

@csrf_exempt
def deleteImage(request):
	if request.method == "POST":
		urlToImage = request.body
		print urlToImage
                drawings = Drawing.objects.all()
		for drawing in drawings:
			if drawing.image.url == urlToImage:
				drawing.delete()
				return HttpResponse('Success')

@csrf_exempt
def comment(request):
	if request.method == "POST":
		i = request.META['HTTP_IMAGEURL']
		imageURL = i[7:]
		try:
			drawingForComment = Drawing.objects.get(image=imageURL)
		except Drawing.DoesNotExist:
			return HttpResponse('Failure')
		newComment = Comment(drawing = drawingForComment, comment = request.body, isNotified=False)
		newComment.save()
		drawingForComment.shouldNotify = True
		drawingForComment.save()
		return HttpResponse('comment success.')

@csrf_exempt
def get_new_comments(request):
	if request.method == "POST":
		i = request.body
		imageURL = i[7:]
		try:
			drawingForComment = Drawing.objects.get(image=imageURL)
		except Drawing.DoesNotExist:
			return HttpResponse('')

		comments = drawingForComment.comment_set.all()
		if (comments.count() < 1):
			return HttpResponse('')
		newComments = comments.filter(isNotified=False)
		if (newComments.count() < 1):
			return HttpResponse('')
		newComment = newComments[0]
                newComment.isNotified = True
		newComment.save()
		return HttpResponse(newComment)

@csrf_exempt
def get_all_comments(request):
	if request.method == "POST":
		i = request.body
		imageURL = i[7:]
		try:
			drawingForComment = Drawing.objects.get(image=imageURL)
		except Drawing.DoesNotExist:
			return HttpResponse()

		comments = drawingForComment.comment_set.all()
		if (comments.count() < 1):
			return HttpResponse()
		print imageURL
		commentID = request.META['HTTP_COMMENTID']
		newComments = comments.filter(id__gt=int(commentID))
		if (newComments.count() < 1):
			return HttpResponse()
		newComment = newComments[0]
		response = HttpResponse(newComment)
		response['commentID'] = newComment.id
		return response

@csrf_exempt
def get_likes(request):
	if request.method == "POST":
		i = request.body
		imageURL = i[7:]
		try:
			drawingForLike = Drawing.objects.get(image=imageURL)
		except Drawing.DoesNotExist:
			return HttpResponse()
		like = drawingForLike.likes
		return HttpResponse(like)

@csrf_exempt
def get_default_image(request):
	if request.method == "POST":
		i = request.body
		imageURL = i[7:]
		try:
			drawingToSend = Drawing.objects.get(image=imageURL)
		except Drawing.DoesNotExist:
			return HttpResponse()

		response = HttpResponse(content_type="image/png")
		url = settings.BASE_DIR + drawingToSend.image.url
		image = Image.open(url)
		image.save(response, 'png')
		return response
