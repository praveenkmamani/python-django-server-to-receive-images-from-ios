from django.contrib import admin
from ParseApp.models import Drawing, Comment

class DrawingAdmin(admin.ModelAdmin):
	list_display = ('id', 'image', 'likes', 'userID','newLikes','shouldNotify',) 

class CommentAdmin(admin.ModelAdmin):
	list_display = ('drawing', 'comment', 'isNotified',)

admin.site.register(Drawing, DrawingAdmin)
admin.site.register(Comment, CommentAdmin)
