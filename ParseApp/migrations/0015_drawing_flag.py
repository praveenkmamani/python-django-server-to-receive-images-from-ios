# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0014_drawing_userid'),
    ]

    operations = [
        migrations.AddField(
            model_name='drawing',
            name='flag',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
