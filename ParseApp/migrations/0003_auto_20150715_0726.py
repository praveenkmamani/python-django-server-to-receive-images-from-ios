# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ParseApp', '0002_auto_20150715_0658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drawing',
            name='photo',
            field=models.ImageField(upload_to=b''),
            preserve_default=True,
        ),
    ]
